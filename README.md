# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    Open HTMl and the mode will first be pen mode, 
    as you click any other button then the mode will turn to the mode you.
    At the same time, your mode button will become blue.

### Function description

    1 . Shape buttons : 
        Rect : To draw a rectangle.
        Circle : To draw a circle.
        Triangle : To draw a triangle.
    2 . Text relative buttons :
        You can draw text by clicking anywhere of canvas. 
        Futhermore, you can change the font size and font family through the
        selector and form below.
    3 . Eraser
        You can clear the past trace you drew.
        As yu can also clear it by shape of rect, circle and triangle,
        not only line.
    4 . Undo / Redo
        You can go back to the last trace you draw, and you can recover the further
        canvas you've undone by redo button.
    5 . Color-selector
        You can choose the color by clicking this.
    6 . Brush size
        You can change your brush size by dragging this.
    7 . Clear
        You can clear the whole canvas , restart.
    8 . Save
        You can directly download this picture by clicking this button.
    9 . Ditto
        As you click this button, the picture of ditto will play the GIF. 
        The next time you click it, ditto will stop. 

### Gitlab page link

    your web page URL, which should be "https://[studentID].gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    Anythinh you want to say to TAs.
    TAs you just typed wrong the "Anything" word. :(

