const canvas = document.getElementById("canvas");

canvas.width = 800;
canvas.height = 400;

//canvas.style.background = "white";

canvas.style.cursor = "url('./src/pen2.png') , auto";

let context = canvas.getContext("2d");
context.fillStyle = "white";
//context.fillRect(0 , 0 , canvas.width , canvas.height);


let start_background_color = "white";
let draw_color = "black";
let draw_width = "2";
let draw_text = "T";
let is_drawing = false;

var font_size = 14;
var font_kind = 'sans-serif';
var has_input = false;
var text_go = false;

let last_color = "red";
let count = 0;

let rect_ = 0;
let circle_ = 0;
let triangle_ = 0;

let restore_array = [];
let index = -1;

let state = 1;

function change_color(element){
    draw_color = element.style.background;
}

canvas.addEventListener("touchstart" , start , false);
canvas.addEventListener("touchmove" , draw , false);
canvas.addEventListener("mousedown" , start , false);
canvas.addEventListener("mousemove" , draw , false);

canvas.addEventListener("touchend" , stop , false);
canvas.addEventListener("mouseup" , stop , false);
canvas.addEventListener("mouseout" , stop , false);


let sx = -1 , sy = -1;
let widthx = 0 , heighty = 0;

function start(event){
    is_drawing = true;
    context.beginPath();
    sx = event.clientX - canvas.offsetLeft;
    sy = event.clientY - canvas.offsetTop;
    context.moveTo(event.clientX - canvas.offsetLeft ,
                    event.clientY - canvas.offsetTop);
    event.preventDefault();
}



function draw(event){
    if(is_drawing){
        //canvas.style.cursor = "url('./src/pen2.png') , auto";
        context.strokeStyle = draw_color;
        context.lineWidth = draw_width;
        context.lineCap = "round";
        context.lineJoin = "round";
        var widthx_n =  (event.clientX - canvas.offsetLeft - sx);
        var heighty_n = (event.clientY - canvas.offsetTop - sy);
        var ssx = event.clientX - canvas.offsetLeft;
        var ssy = event.clientY - canvas.offsetTop;
        if(rect_ == 1 && triangle_ == 0 && circle_ == 0){
            //context.strokeRect( sx , sy , widthx_n , heighty_n);
            if(index != -1){
                console.log(index);
                context.putImageData(restore_array[index] , 0 , 0);
            }else context.clearRect(0 , 0 , canvas.width , canvas.height);
            widthx = widthx_n;
            heighty = heighty_n;
            context.strokeRect( sx , sy , widthx_n , heighty_n);
            //context.stroke();
        }else if(triangle_ == 1 && rect_ == 0 && circle_ == 0){
            if(index != -1){
                console.log(index);
                context.putImageData(restore_array[index] , 0 , 0);
            }else context.clearRect(0 , 0 , canvas.width , canvas.height);
            // context.lineTo(ssx , ssy);
            // context.lineTo(ssx - 2*(ssx - sx) , ssy);
            // context.lineTo(sx , sy);
            context.beginPath();
            context.lineTo(event.clientX - canvas.offsetLeft - 2*(event.clientX - canvas.offsetLeft - sx) ,
                event.clientY - canvas.offsetTop);
            context.lineTo(sx , sy);
            context.lineTo(event.clientX - canvas.offsetLeft , 
                event.clientY - canvas.offsetTop);
            context.lineTo(event.clientX - canvas.offsetLeft - 2*(event.clientX - canvas.offsetLeft - sx) ,
                event.clientY - canvas.offsetTop);
            context.lineTo(sx , sy);
            context.stroke();
            //context.putImageData(restore_array[index-1] , 0 , 0);
            //context.fill();
        }else if(circle_ == 1 && rect_ == 0 && triangle_ == 0){
            if(index != -1){
                context.putImageData(restore_array[index] , 0 , 0);
            }else context.clearRect(0 , 0 , canvas.width , canvas.height);
            var start_arc = (Math.PI/180) * 0;
            var end_arc = (Math.PI/180) * 360;
            //context.moveTo(event.clientX - canvas.offsetLeft , event.clientY - canvas.offsetTop);
            context.beginPath();
            context.arc((event.clientX - canvas.offsetLeft - sx) / 2 + sx , (event.clientY - canvas.offsetTop - sy) / 2 + sy ,
                 Math.sqrt(Math.pow(event.clientX - canvas.offsetLeft - sx , 2) + Math.pow(event.clientY - canvas.offsetTop - sy , 2)) / 2 ,
                 start_arc , end_arc , 1);
            context.stroke();
            //context.putImageData(restore_array[index-1] , 0 , 0);
        }else{
            context.lineTo(event.clientX - canvas.offsetLeft , event.clientY - canvas.offsetTop);
            context.stroke();
        }
    }
    event.preventDefault();
    console.log(index);
}





function stop(event){
    if(is_drawing){
        var widthx_nn =  (event.clientX - canvas.offsetLeft - sx);
        var heighty_nn = (event.clientY - canvas.offsetTop - sy);
        context.stroke();
        context.closePath();
        is_drawing = false;
        if(event.type != "mouseout"){
            if(index < restore_array.length - 1) index = restore_array.length - 1;
            console.log(index);
            index += 1;
            console.log(index);
            restore_array.splice(index , 0 , context.getImageData(0 , 0 , canvas.width , canvas.height));
        }
        console.log(index);
    }
    event.preventDefault();

    

}

function Save(element){
    var img = canvas.toDataURL('image/png');
    if(window.navigator.msSaveBlob){
        window.navigator.msSaveBlob(canvas.msToBlob() , "canvas-image.png");
    }else{
        const a = document.createElement("a");
        document.body.appendChild(a);
        a.href = canvas.toDataURL();
        a.download = "canvas-image.png";
        a.click();
        document.body.removeChild(a);
    }
    //document.write('<img src="'+img+'"/>');
    //element.href = img;
}

function clear_canvas(){
    context.fillStyle = start_background_color;
    context.clearRect(0 , 0 , canvas.width , canvas.height);
    //context.fillRect(0 , 0 , canvas.width , canvas.height);
    // rect_ = 0; triangle_ = 0; circle_ = 0;
    restore_array = [];
    index = -1;
}

function undo(){
    if(index > 0){
        //restore_array.pop();
        index -= 1;
        context.putImageData(restore_array[index] , 0 , 0);
    }else{
        context.fillStyle = start_background_color;
        context.clearRect(0 , 0 , canvas.width , canvas.height);
        context.fillRect(0 , 0 , canvas.width , canvas.height);
        index = -1;
    }
    console.log(index);
}

function redo(){
    if(index < restore_array.length - 1){
        index += 1;
        context.putImageData(restore_array[index] , 0 , 0);
    }
}

let last_cur = "pointer";

function Eraser(){
    var target = document.getElementById(eraser);
    if(count == 1){
        if(rect_ || circle_ || triangle_){
            canvas.style.cursor = "crosshair";
        }else{
            canvas.style.cursor = "url('./src/pen2.png') , auto";
        }
        if(text_go){
            canvas.style.cursor = "url('./src/text.png') , auto";
        }
        draw_color = last_color;
        context.globalCompositeOperation = "source-over";
        document.getElementById('eraser').style.background = "slategray";
        //element.style.backgroundColor = "slategray";
        count = 0;
    }else{
        last_cur = canvas.style.cursor;
        document.getElementById('eraser').style.background = "blue";
        document.getElementById('text').style.background = "slategray";
        canvas.style.cursor = "url('./src/eraser2.png') , auto";
        last_color = draw_color;
        //draw_color = start_background_color;
        context.globalCompositeOperation = "destination-out";
        text_go = false;
        //element.backgroundColor = "plum";
        count = 1;
    }
}

function Rect(){
    if(rect_){
        rect_ = 0;
        document.getElementById('rect').style.background = "slategray";
        if(count == 0){
            canvas.style.cursor = "url('./src/pen2.png') , auto";
        }
    }else{
        rect_ = 1;
        circle_ = 0;
        triangle_ = 0;
        text_go = false;
        document.getElementById('text').style.background = "slategray";
        document.getElementById('rect').style.background = "blue";
        document.getElementById('circle').style.background = "slategray";
        document.getElementById('triangle').style.background = "slategray";
        if(count == 0){
            canvas.style.cursor = "crosshair";
        }
    }
}

function Circle(){
    if(circle_){
        circle_ = 0;
        document.getElementById('circle').style.background = "slategray";
        if(count == 0){
            canvas.style.cursor = "url('./src/pen2.png') , auto";
        }
    }else{
        rect_ = 0;
        circle_ = 1;
        triangle_ = 0;
        text_go = false;
        document.getElementById('text').style.background = "slategray";
        document.getElementById('circle').style.background = "blue";
        document.getElementById('rect').style.background = "slategray";
        document.getElementById('triangle').style.background = "slategray";
        if(count == 0){
            canvas.style.cursor = "crosshair";
        }
    }
}

function Triangle(){
    if(triangle_){
        triangle_ = 0;
        document.getElementById('triangle').style.background = "slategray";
        if(count == 0){
            canvas.style.cursor = "url('./src/pen2.png') , auto";
        }
    }else{
        rect_ = 0;
        circle_ = 0;
        triangle_ = 1;
        text_go = false;
        document.getElementById('text').style.background = "slategray";
        document.getElementById('triangle').style.background = "blue";
        document.getElementById('rect').style.background = "slategray";
        document.getElementById('circle').style.background = "slategray";
        if(count == 0){
            canvas.style.cursor = "crosshair";
        }
    }
}

const EL = (sel) => document.querySelector(sel);
const ctx = EL("#canvas").getContext("2d");

function readImage() {
  if (!this.files || !this.files[0]) return;
  
  const FR = new FileReader();
  FR.addEventListener("load", (evt) => {
    const img = new Image();
    img.addEventListener("load", () => {
      ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
      var ori = ctx.fillStyle;
      ctx.fillStyle = start_background_color;
      ctx.fillRect(0 , 0 , ctx.canvas.width , ctx.canvas.height);
      ctx.drawImage(img, 0, 0);
      if(index < restore_array.length - 1){
        index = restore_array.length - 1;
      }
      index += 1;
      console.log(restore_array.length);
      console.log(index);
      restore_array.splice(index , 0 , context.getImageData(0 , 0 , canvas.width , canvas.height));
    });
    img.src = evt.target.result;
  });
  FR.readAsDataURL(this.files[0]);
  ctx.fillStyle = draw_color;
  if(index < restore_array.length - 1){
      index = restore_array.length - 1;
  }
  index += 1;
  console.log(restore_array.length);
  console.log(index);
  restore_array.splice(index , 0 , context.getImageData(0 , 0 , canvas.width , canvas.height));
}

EL("#fileUpload").addEventListener("change", readImage);

canvas.onclick = function(e) {
    if (has_input || !text_go) return;
    addInput(e.clientX, e.clientY);
}

function Text_go(){
    if(text_go == false){
        text_go = true;
        canvas.style.cursor = "url('./src/text.png') , auto";
        if(count == 1) Eraser();
        rect_ = 0; circle_ = 0; triangle_ = 0;
        document.getElementById('text').style.background = "blue";
        document.getElementById('rect').style.background = "slategray";
        document.getElementById('circle').style.background = "slategray";
        document.getElementById('triangle').style.background = "slategray";
        document.getElementById('eraser').style.background = "slategray";
    }else{
        text_go = false;
        canvas.style.cursor = "url('./src/pen2.png') , auto";
        document.getElementById('text').style.background = "slategray";
    }
}


function addInput(x, y) {
    var input = document.createElement('input');
    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = (x - canvas.offsetLeft) + 'px';
    input.style.top = (y - canvas.offsetTop) + 'px';
    input.onkeydown = handleEnter;
    document.body.appendChild(input);
    input.focus();
    has_input = true;
}

function handleEnter(e) {
    var keyCode = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;;
    if (keyCode === 13) {
        drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        console.log(this.value);
        document.body.removeChild(this);
        has_input = false;
    }
}

function drawText(txt, x, y) {
    console.log(txt);
    context.textBaseline = 'top';
    context.textAlign = 'left';
    context.font = font_size + "px" + " " + font_kind;
    context.fillStyle = draw_color;
    context.fillText(txt , x - canvas.offsetLeft , y - canvas.offsetTop);
    if(index < restore_array.length - 1) index = restore_array.length - 1;
        index += 1;
        console.log(index);
        restore_array.splice(index , 0 , context.getImageData(0 , 0 , canvas.width , canvas.height));
}

var ditto_ = false;
let cur = "pointer";

let raf;
var start = false;

function Ditto(){
    if(start){
        document.getElementById("video").pause();
        document.getElementById('ditto').style.background = "slategray";
        start = false;
    }else{
        start = true;
        document.getElementById("video").play();
        document.getElementById('ditto').style.background = "blue";
    }
}




